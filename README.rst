py_package
==========
.

.. image:: https://img.shields.io/bitbucket/pipelines/mul118/py_package.svg
   :target: https://bitbucket.org/mul118/py_package/addon/pipelines/home

A skeleton of a Python package with test suite included.
   
.. image:: https://farm4.staticflickr.com/3951/15672691531_3037819613_o_d.png

Customization quick start
-------------------------

To use py_package as the start of a new project, do the following, preferably in
a virtual environment. Clone the repo.

.. code-block:: console

    git clone https://bitbucket.org/mul118/py_package myproject
    cd myproject

Replace all occurrences of 'py_package' with the name of your own project.
(Note: the commands below require bash, find, and sed and are yet tested only on OS X.)

.. code-block:: console

    if [ -d py_package ]; then find . -not -path './.git*' -type f -exec sed -i '' -e 's/py_package/myproject/g' {} + ; fi
    mv py_package myproject

Then install in locally editable (``-e``) mode and run the tests.

.. code-block:: console

    pip install -e .[test]
    py.test
