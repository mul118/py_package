.. py_package documentation master file, created by
   sphinx-quickstart on Fri Aug 17 17:36:53 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to py_package's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2
  
   readme

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

