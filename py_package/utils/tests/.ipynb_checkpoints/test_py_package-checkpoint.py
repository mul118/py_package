import py_package


def test_is_string():
    s = py_package.joke()
    assert(isinstance(s, basestring))
