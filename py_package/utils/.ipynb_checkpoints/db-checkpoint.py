import os 
import configparser
import sqlalchemy
import config as cfg


def pg_connect(user, password, db, host='localhost', port=5432):
    '''Returns a connection and a metadata object'''

    url = 'postgresql://{}:{}@{}:{}/{}'
    url = url.format(user, password, host, port, db)

    con = sqlalchemy.create_engine(url, client_encoding='utf8')

    return con

def berimbolo_connect(db):
    '''Convenience function to connect to specified db in Berimbolo.'''
    config = cfg.get_config()
    con = pg_connect(
        config['db']['user'],
        config['db']['passwd'],
        db,
        config['db']['host'],
        port=5432)
  
    return con
