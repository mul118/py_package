import sys, os
import inspect
import configparser
from shutil import copyfile
import py_package as pp

# TODO: 
# 1. Make it work within package context (already works?)
# 2. Write unit tests


def list_configs():
    """List configs.

    List all available configs for the package.

    Returns:
        list: list of available configs        
        
    """
    pkg_dir = os.path.dirname(inspect.getfile(pp))
    configs = os.listdir(pkg_dir+'/conf')
    config_names = [x.replace('.ini','') for x in configs if (not x == '__active__.ini' and x.endswith('.ini'))]
    
    return config_names
      
    
def set_config(name = 'local'):
    
    """Set active config

    Set active config to that specified, if available. Config is set to 'local' by default.

    Args:
        name (str): Name of the config

    """
    pkg_dir = os.path.dirname(inspect.getfile(pp))
    conf_filepath = pkg_dir + '/conf/' + name + '.ini'
    active_filepath = pkg_dir + '/conf/__active__.ini'
    
    if not os.path.isfile(conf_filepath):
        print('Config ' + name + 'does not exist')
    else:
        copyfile(conf_filepath, active_filepath)
        print('Config set to ' + name)
        

def get_config():
    """Get active config

    Get active config, initializing if necessary.

    Returns:
        configparser.ConfigParser:  config object

    """
    pkg_dir = os.path.dirname(inspect.getfile(pp))
    conf_filepath = pkg_dir + '/conf/__active__.ini'
    
    if not os.path.isfile(conf_filepath):
        set_config()
        
    conf = configparser.ConfigParser()
    conf.read(conf_filepath)
    
    return conf
    
