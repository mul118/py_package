#!/usr/bin/env python
from setuptools import setup
import os

def readme():
    with open('README.rst') as f:
        return f.read()
    
conf_files = ['py_package/conf/' + x for x in os.listdir('py_package/conf') if x.endswith('.ini')]

setup(name='py_package',
      version='0.1',
      description='Testbed package for various features',
      url='http://github.com',
      author='Misha Lisovich',
      license='MIT',
      packages=['py_package', 'py_package.conf', 'py_package.utils'],
      install_requires=[
        'markdown',
      ],
      data_files=[('py_package.conf', conf_files)],
      include_package_data=True,
      zip_safe=False)
